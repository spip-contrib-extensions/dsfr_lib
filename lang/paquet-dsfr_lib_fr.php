<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		MIT - https://github.com/GouvernementFR/dsfr/blob/main/LICENSE.md
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dsfr_lib_description' => '
Le Système de Design de l\'État (ci-après, le DSFR) est un ensemble de composants web HTML, CSS et Javascript pour faciliter le travail des équipes projets des sites Internet publics, et créer des interfaces numériques de qualité et accessibles.

Le plugin {{DSFR Lib}} fournit uniquement les scripts CSS, le Javascript et les images (icônes, favicons et pictogrammes) permettant d\'utiliser l\'ensemble des composants DSFR. Vous devrez ensuite adapter le contenu de vos squelettes pour pouvoir utiliser ces différents composants.

Le but du plugin est de fournir uniquement la librairie DSFR pour facilement proposer des mises à jour lors de la sortie d\'une nouvelle version du DSFR.
	',
	'dsfr_lib_nom' => 'DSFR Lib',
	'dsfr_lib_slogan' => 'Librairie du Système de Design de l\'État',
);